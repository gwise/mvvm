﻿using System.Diagnostics;
using System.Windows.Input;
using Xamarin.Forms;

namespace MVVM
{
    public class MainPageViewModel
    {
        private bool isEnable = true;
        public ICommand SaveCommand { get; }
        public ICommand Save2Command { get; }

        public MainPageViewModel()
        {
            SaveCommand = new Command(
                execute: () =>
                {
                    isEnable = false;
                    RefreshSaveCanExecutes();
                    Save();
                },
                canExecute: () =>
                {
                    return isEnable;
                }
            );

            Save2Command = new Command<string>((obj) => Save2(obj));
        }
        private void Save()
        {
            Debug.WriteLine("Save Command");
        }

        private void RefreshSaveCanExecutes()
        {
            (SaveCommand as Command).ChangeCanExecute();
        }
        private void Save2(string obj)
        {
            Debug.WriteLine("Save2 Command");
            Debug.WriteLine(obj);
        }
    }
}
